package com.hakkiyavuz.codemania.hanhan;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class Control_Activity extends Activity implements View.OnTouchListener{

    private static final String TAG = "HanHan";
    private Button forward,backward,turn_right,turn_left;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);

        initComponents();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_control, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if(view.equals(forward)){

            if(MotionEvent.ACTION_DOWN == motionEvent.getAction()){

                Log.i(TAG,"Forward button is pressing...");

            }else if(MotionEvent.ACTION_UP == motionEvent.getAction()){
                Log.i(TAG,"Forward button is released.");

            }
        }

        if(view.equals(backward)){

            if(MotionEvent.ACTION_DOWN == motionEvent.getAction()){

                Log.i(TAG,"Backward button is pressing...");

            }else if(MotionEvent.ACTION_UP == motionEvent.getAction()){
                Log.i(TAG,"Backward button is released.");

            }
        }

        if(view.equals(turn_right)){

            if(MotionEvent.ACTION_DOWN == motionEvent.getAction()){

                Log.i(TAG,"Right button is pressing...");

            }else if(MotionEvent.ACTION_UP == motionEvent.getAction()){
                Log.i(TAG,"Right button is released.");

            }
        }

        if(view.equals(turn_left)){

            if(MotionEvent.ACTION_DOWN == motionEvent.getAction()){

                Log.i(TAG,"Left button is pressing...");


            }else if(MotionEvent.ACTION_UP == motionEvent.getAction()){
                Log.i(TAG,"Forward button is released.");

            }
        }


        return false;
    }

    private void initComponents(){

        forward = (Button) findViewById(R.id.forward);
        backward = (Button) findViewById(R.id.backward);
        turn_left = (Button) findViewById(R.id.left);
        turn_right = (Button) findViewById(R.id.right);

        forward.setOnTouchListener(this);
        backward.setOnTouchListener(this);
        turn_left.setOnTouchListener(this);
        turn_right.setOnTouchListener(this);




    }


    @Override
    protected void onDestroy() {

        super.onDestroy();
    }


}
